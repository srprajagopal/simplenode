#!/usr/bin/env python3
import time
import sched
import signal

class TimedFunc:
    '''
        An object that encapsulates a function, the frequency to call the function, and a counter (which resets at max)
    
    Args:
        func: the function to call
        delay: in counts (if scheduler runs at 1ms, then the function is executed every counts * 1ms)
    '''
    def __init__(self, func, delay):
        self.func    = func
        self.delay   = delay # can only ms delays for now
        self.counter = 0

    def update(self, args):
        '''
            Called by the scheduler. If counter is not equal to delay or greater than delay, then simply update counter and return. Here we update the counter by 1. This corresponds to an increment of 1 in the scheduler's time base.
        '''
        if self.counter <= self.delay:
            self.counter += 1
            return
    
        else:
            self.counter = 0
            self.func(args)

class EventsManager:
    '''
        The publishers need to timed and called. I'm using the sched library for this.
        This class is a wrapper around the sched library maintaining an array of functions. These functions will be called periodically based on the given frequency. Important part is to maintain a counter for each. This counter will be reset after it reaches its max (period).
    Args:
        funcarr: array of TimedFuncs (see TimedFunc)
    '''
    def __init__(self, funcarr):
        self.funcarr = funcarr
        self.scheduler = sched.scheduler(time.time, time.sleep)
    
    def start(self):
        '''
            Starts the event manager i.e. starts looping over at 1ms intervals. 
        '''
        self.running = True
        
        while self.running:
            self.scheduler.enter(0.01, 1, EventsManager.calls, argument=(self.funcarr,)) 
            self.scheduler.run()

        # running is False
        # cancel all jobs here
        for evt in self.scheduler.queue:
            self.scheduler.cancel(evt) 

    @staticmethod
    def calls(funcarr):
        ''' 
            performs looping over the defined timedfuncs to get their return data

            Args:
                funcarr: the array of timedfuncs

            Dev comments:
            perform publishing here
            the update function has to be called with all the data available
            ex: A publisher might need three different subscribers data
            sending all the data to the publisher allows it to define it's output.
            TODO: how to use the return data from the update (which is the publisher's output) and send it to the actual publisher?
                one way is to insert the publish call in the SimpleNode object before invoking eventsmanager. Main node gives a callback, which is converted into a TimedFunc by SimpleNode
        '''
        for i, f in enumerate(funcarr):
            f.update(*[i])

    def stop(self):
        self.running = False
        print("Stopping jobs...")
         
if __name__ == '__main__':
    def printer(args):
        print("{:d}: {:f}".format(args, time.time()))

    funcarr = [ TimedFunc(printer, 1), TimedFunc(printer, 2) ]
    em = EventsManager(funcarr)
    signal.signal(signal.SIGINT, lambda sig, frame: em.stop())
    em.start()
