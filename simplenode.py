import rospy
from threading import Thread
import signal
import sched, time
from eventsmanager import TimedFunc, EventsManager

class SimpleNode:
    def __init__(self, namespace, names_cbs, names_outs):
        self.namespace = namespace
        rospy.init_node(self.namespace, anonymous=True)

        # create subscribers
        self.subs = self.subscribers(names_cbs)

        # create publishers
        self.pubs = self.publishers(names_outs)

    def start(self):
        # create signal for shutdown
        rospy.loginfo("Starting publisher threads now...")
        signal.signal(signal.SIGINT, lambda sig, frame: self.stop())

        # create timedfuncs for publishers
        def timedcallback(publisher, user_cb):
            user_cb
             
        # array of lambdas
        #   each lambda calls user function first, then publishes the result
        lambdas = [lambda args: p['pub'].publish(p['callback']()) for p in self.pubs]

        # create timedfuncs arr
        funcarr = [TimedFunc(foo, p['delay']) for foo, p in zip(lambdas, self.pubs)]

        # create events manager
        self.em = EventsManager(funcarr)
        
        # start events manager
        self.t_em = Thread(target=self.em.start)
        
        # use events manager
        self.t_em.start()

        rospy.spin()

    def stop(self):
        self.em.stop()
        self.running = False
        rospy.signal_shutdown('user abort')
 
    def subscribers(self, names_cbs):
        for name_cb in names_cbs:
            if 'namespace' in name_cb:
                rospy.Subscriber('{:s}/{:s}'.format(self.namespace, name_cb["channel_name"]), name_cb["data_type"], name_cb["callback"], queue_size=name_cb["queue_size"])
            else:
                rospy.Subscriber(name_cb["channel_name"], name_cb["data_type"], name_cb["callback"], queue_size=name_cb["queue_size"])

    def publishers(self, names_outs): 
        arr = []
        for name_out in names_outs:
            name_out['pub'] = rospy.Publisher('{:s}/{:s}'.format(self.namespace, name_out["channel_name"]), name_out["data_type"], queue_size=name_out["queue_size"])
            arr.append(name_out)

        return arr
