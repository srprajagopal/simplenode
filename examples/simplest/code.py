# tag::imports[]
from simplenode import SimpleNode
from std_msgs.msg import Int32, UInt32
# end::imports[]

# tag::callbackSubscriber[]
def foo(data): # Int32 <1>
    # use data here
    pass
# end::callbackSubscriber[]

# tag::callbackPublisher[]
def pubcall(): # <2>
    # perform publishing here
    pass
# end::callbackPublisher[]

# tag::call[]
node = SimpleNode('myNameSpace', # <1> 
            [ # <2>
                {
                    'channel_name' : "subscFoo", # <3>
                    'callback'     : foo, # <4>
                    'queue_size'   : 10, # <5>
                    'data_type'    : Int32 # <6>
                }, # more subscribers <7>
            ],
            [ # <8>
                {
                    'channel_name' : "myPub", # <9>
                    'delay'        : 1 # <10>
                    'callback'     : pubcall, # <11>
                    'queue_size'   : 10, # <12>
                    'data_type'    : UInt32, # <13> 
                }, # more publishers <14>
            ])
node.start() # <15>
# end::call[]
